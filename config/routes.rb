Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'users/registrations' }

  root 'application#index'

  resources :transactions,          only: [:show, :index]
  resources :users,                 only: :index
  resources :payments,              only: [:new, :create, :index, :show]
  resources :withdrawals,           only: [:new, :create, :index, :show]
  resources :payment_gateways,      only: :create
  resources :withdrawal_gateways,   only: :create
end
