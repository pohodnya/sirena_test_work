# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170705121421) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "payments", force: :cascade do |t|
    t.integer  "sum",        default: 0,                 null: false
    t.integer  "balance",    default: 0,                 null: false
    t.string   "gateway",    default: "Сбербанк Онлайн"
    t.string   "reason"
    t.string   "state"
    t.integer  "user_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.index ["user_id"], name: "index_payments_on_user_id", using: :btree
  end

  create_table "transactions", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.integer  "sum",            default: 0, null: false
    t.string   "message"
    t.string   "state"
    t.integer  "debit_balance"
    t.integer  "credit_balance"
    t.string   "debit_type"
    t.integer  "debit_id"
    t.string   "credit_type"
    t.integer  "credit_id"
    t.string   "subject_type"
    t.integer  "subject_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["credit_type", "credit_id"], name: "index_transactions_on_credit_type_and_credit_id", using: :btree
    t.index ["debit_type", "debit_id"], name: "index_transactions_on_debit_type_and_debit_id", using: :btree
    t.index ["subject_type", "subject_id"], name: "index_transactions_on_subject_type_and_subject_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "phone",                  default: "",    null: false
    t.string   "name",                   default: "",    null: false
    t.string   "surname",                default: "",    null: false
    t.string   "patronymic",             default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.integer  "balance",                default: 0,     null: false
    t.string   "reset_password_token"
    t.boolean  "has_access_to_crm",      default: false, null: false
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["phone"], name: "index_users_on_phone", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "withdrawals", force: :cascade do |t|
    t.integer  "sum",        default: 0, null: false
    t.integer  "balance",    default: 0, null: false
    t.string   "reason"
    t.string   "state"
    t.integer  "user_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["user_id"], name: "index_withdrawals_on_user_id", using: :btree
  end

end
