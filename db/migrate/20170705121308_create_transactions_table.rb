class CreateTransactionsTable < ActiveRecord::Migration[5.0]
  def change
    create_table "transactions", id: :uuid, force: :cascade do |t|
      t.integer "sum",        default: 0, null: false
      t.string  "message"
      t.string  "state"
      t.integer "debit_balance"
      t.integer "credit_balance"
      t.references :debit, polymorphic: true, index: true
      t.references :credit, polymorphic: true, index: true
      t.references :subject, polymorphic: true, index: true
      t.timestamps
    end
  end
end
