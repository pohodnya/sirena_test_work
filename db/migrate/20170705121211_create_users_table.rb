class CreateUsersTable < ActiveRecord::Migration[5.0]
  def change
    enable_extension 'plpgsql'
    enable_extension 'uuid-ossp'

    create_table "users", force: :cascade do |t|
      t.string   "email",                  default: "", unique: true, null: false
      t.string   "phone",                  default: "", unique: true, null: false
      t.string   "name",                   default: "", null: false
      t.string   "surname",                default: "", null: false
      t.string   "patronymic",             default: "", null: false
      t.string   "encrypted_password",     default: "", null: false
      t.integer  "balance",                default: 0, null: false
      t.string   "reset_password_token"
      t.boolean  "has_access_to_crm",      default: false, null: false
      t.datetime "reset_password_sent_at"
      t.datetime "remember_created_at"
      t.integer  "sign_in_count",          default: 0,  null: false
      t.datetime "current_sign_in_at"
      t.datetime "last_sign_in_at"
      t.inet     "current_sign_in_ip"
      t.inet     "last_sign_in_ip"
      t.datetime "created_at",                          null: false
      t.datetime "updated_at",                          null: false
      t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
      t.index ["phone"], name: "index_users_on_phone", unique: true, using: :btree
      t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    end
  end
end
