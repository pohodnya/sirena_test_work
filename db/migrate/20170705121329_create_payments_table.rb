class CreatePaymentsTable < ActiveRecord::Migration[5.0]
  def change
    create_table "payments", force: :cascade do |t|
      t.integer "sum",        default: 0, null: false
      t.integer "balance",    default: 0, null: false
      t.string  "gateway",    default: 'Сбербанк Онлайн'
      t.string  "reason"
      t.string  "state"
      t.references :user, index: true
      t.timestamps
    end
  end
end
