require 'rails_helper'


RSpec.describe PaymentsController, type: :controller do

  let(:user) { create :user, balance: 1000}
  before {sign_in user}

  let(:valid_attributes) {
    {
        user_id: user.to_param,
        sum: 100
    }
  }

  let(:invalid_attributes) { {sum: 1} }


  let(:valid_session) { {} }

  describe "GET #index" do
    it "returns a success response" do
      payment = Payment.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      payment = Payment.create! valid_attributes
      get :show, params: {id: payment.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Payment" do
        expect {
          post :create, params: {payment: valid_attributes}, session: valid_session
        }.to change(Payment, :count).by(1)
      end

      it "redirects to the created payment" do
        post :create, params: {payment: valid_attributes}, session: valid_session
        expect(response).to redirect_to(Payment.last)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {payment: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

end
