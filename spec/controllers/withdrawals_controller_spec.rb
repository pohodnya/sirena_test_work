require 'rails_helper'

RSpec.describe WithdrawalsController, type: :controller do

  let(:user) { create :user, balance: 1000}
  before {sign_in user}
  let(:valid_attributes) {
    {
        user_id: user.to_param,
        sum: 100,
        reason: "Хочу денег"
    }
  }

  let(:invalid_attributes) { {user_id: user.to_param, sum: 1200} }

  let(:valid_session) { {} }

  describe "GET #index" do
    it "returns a success response" do
      withdrawal = Withdrawal.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      withdrawal = Withdrawal.create! valid_attributes
      get :show, params: {id: withdrawal.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Withdrawal" do
        expect {
          post :create, params: {withdrawal: valid_attributes}, session: valid_session
        }.to change(Withdrawal, :count).by(1)
      end

      it "redirects to the created withdrawal" do
        post :create, params: {withdrawal: valid_attributes}, session: valid_session
        expect(response).to redirect_to(Withdrawal.last)
      end
    end
  end

end
