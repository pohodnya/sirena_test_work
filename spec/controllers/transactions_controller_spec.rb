require 'rails_helper'

RSpec.describe TransactionsController, type: :controller do

  let(:valid_session) { {} }
  let(:user) { create :user }
  let(:payment) { create :payment }

  before { sign_in user }

  describe 'GET #index' do
    let!(:transaction1) { create :transaction, debit: payment, credit: user, subject: payment}
    let!(:transaction2) { create :transaction, debit: payment, credit: user, subject: payment}
    subject { get :index, session: valid_session }

    before { subject }
    it 'returns a success response' do
      expect(response).to be_success
    end

    it 'return a list of all transactions' do
      expect(assigns(:transactions).length).to eq(2)
    end
  end

  describe 'GET #show' do
    let!(:transaction) { create :transaction, debit: payment, credit: user, subject: payment}
    subject { get :show, params: {id: transaction.to_param}, session: valid_session }

    before { subject }
    it 'returns a success response' do
      expect(response).to be_success
    end

    it 'returns a transaction by id' do
      expect(assigns(:transaction)).to eq(transaction)
    end
  end
end
