require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'user cannot has a negative balance' do
    let!(:user) { create :user }

    subject { user.update(balance: -10) }

    it 'return error' do
      expect{subject}.to change{user.errors.messages.count}.to(1)
    end
  end
end
