require 'rails_helper'

RSpec.describe Transaction, type: :model do
  describe 'withdrawal created with state new' do
    let(:user1) { create :user }
    let(:user2) { create :user }
    let(:payment) { create :payment}
    let(:transaction) { Transaction.create debit: user1, credit: user2, subject: payment }

    it { expect(transaction.state).to eq 'new' }
  end

  describe '#reject' do
    let(:user) { create :user }
    let(:transaction) { create :transaction, sum: 200, credit: user }
    subject { transaction.reject }

    it { expect{subject}.to change{transaction.state}.from('new').to('rejected') }
    it { expect{subject}.not_to change{user.balance} }
  end

  describe '#execute' do
    let(:user) { create :user, balance: 500 }
    let(:transaction) { create :transaction, sum: 200, credit: user }
    subject { transaction.execute }

    it { expect{subject}.to change{transaction.state}.from('new').to('executed') }
    it { expect{subject}.to change{user.balance}.to(700) }
  end
end
