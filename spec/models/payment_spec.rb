require 'rails_helper'

RSpec.describe Payment, type: :model do
  describe 'payment created with state new' do
    let(:user) { create :user }
    let(:payment) { Payment.create user: user }

    it { expect(payment.state).to eq 'new' }
  end

  describe '#reject' do
    let(:user) { create :user }
    let(:payment) { create :payment, sum: 200, user: user }
    subject { payment.reject }

    it { expect{subject}.to change{payment.state}.from('new').to('rejected') }
    it { expect{subject}.not_to change{user.balance} }
  end

  describe '#execute' do
    let(:user) { create :user }
    let(:payment) { create :payment, sum: 200, user: user }
    subject { payment.execute }

    it { expect{subject}.to change{payment.state}.from('new').to('executed') }
    it { expect{subject}.to change{user.balance}.to(200) }
    it 'create a new transaction' do
      subject
      expect(Transaction.count).to eq 1
      expect(Transaction.first.debit).to eq payment
      expect(Transaction.first.credit).to eq user
      expect(Transaction.first.subject).to eq payment
      expect(Transaction.first.sum).to eq payment.sum
    end
  end
end
