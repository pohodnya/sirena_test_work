require 'rails_helper'

RSpec.describe Withdrawal, type: :model do
  describe 'withdrawal created with state new' do
    let(:user) { create :user }
    let(:withdrawal) { Withdrawal.create user: user }

    it { expect(withdrawal.state).to eq 'new' }
  end

  describe '#reject' do
    let(:user) { create :user }
    let(:withdrawal) { create :withdrawal, sum: 200, user: user }
    subject { withdrawal.reject }

    it { expect{subject}.to change{withdrawal.state}.from('new').to('rejected') }
    it { expect{subject}.not_to change{user.balance} }
  end

  describe '#execute' do
    let(:user) { create :user, balance: 500 }
    let(:withdrawal) { create :withdrawal, sum: 200, user: user }
    subject { withdrawal.execute }

    context 'user has full sum' do
      it { expect{subject}.to change{withdrawal.state}.from('new').to('executed') }
      it { expect{subject}.to change{user.balance}.to(300) }
      it 'create a new transaction' do
        subject
        expect(Transaction.count).to eq 1
        expect(Transaction.first.debit).to eq user
        expect(Transaction.first.credit).to eq withdrawal
        expect(Transaction.first.subject).to eq withdrawal
        expect(Transaction.first.sum).to eq withdrawal.sum
      end
    end

    context 'user has not full sum' do
      let(:user) { create :user }
      it { expect(withdrawal.may_execute?).to be_falsey }
    end
  end
end
