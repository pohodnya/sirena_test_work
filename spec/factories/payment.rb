FactoryGirl.define do
  factory :payment do
    balance 0
    sum 100
    association :user
  end
end
