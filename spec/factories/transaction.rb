FactoryGirl.define do
  factory :transaction do
    association :debit, factory: :payment
    association :credit, factory: :user
    association :subject, factory: :payment
  end
end
