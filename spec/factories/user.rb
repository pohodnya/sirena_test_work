FactoryGirl.define do
  factory :user do
    sequence(:email) {|n| "user-#{n}@example.com"}
    password 'password'
    password_confirmation 'password'
    name 'Name'
    surname 'Surname'
    patronymic 'Patronymic'
    sequence(:phone) {|n| "891380011#{n}"}
  end
end
