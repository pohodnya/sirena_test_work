FactoryGirl.define do
  factory :withdrawal do
    balance 0
    sum 100
    association :user
  end
end
