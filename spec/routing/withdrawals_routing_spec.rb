require "rails_helper"

RSpec.describe WithdrawalsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/withdrawals").to route_to("withdrawals#index")
    end

    it "routes to #new" do
      expect(:get => "/withdrawals/new").to route_to("withdrawals#new")
    end

    it "routes to #show" do
      expect(:get => "/withdrawals/1").to route_to("withdrawals#show", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/withdrawals").to route_to("withdrawals#create")
    end

  end
end
