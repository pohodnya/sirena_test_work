class Withdrawal < ApplicationRecord
  include Accountable
  include AASM

  belongs_to :user

  aasm column: :state do
    state :new, initial: true
    state :executed
    state :rejected

    event :reject do
      transitions from: :new, to: :rejected
    end

    event :execute do
      after do
        transaction = Transaction.create debit: user, credit: self, subject: self, sum: sum
        transaction.execute
      end

      transitions from: :new, to: :executed, if: :can_withdrawal?
    end
  end

  def can_withdrawal?
    user.balance >= sum
  end
end
