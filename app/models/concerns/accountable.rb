module Accountable
  extend ActiveSupport::Concern

  included do
    has_many :debit_transactions,  class_name: 'Transaction', as: :debit
    has_many :credit_transactions, class_name: 'Transaction', as: :credit
  end
end
