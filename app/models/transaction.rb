class Transaction < ApplicationRecord
  include AASM

  belongs_to :credit,  polymorphic: true
  belongs_to :debit,   polymorphic: true
  belongs_to :subject, polymorphic: true

  validates_presence_of :credit, :debit

  aasm column: :state do
    state :new, initial: true
    state :executed
    state :rejected

    event :reject do
      transitions from: :new, to: :rejected
    end

    event :execute do
      before do
        debit.increment(:balance, - sum)
        debit.save
        update(debit_balance: debit.balance)
      end

      transitions from: :new, to: :executed

      after do
        credit.increment(:balance, sum)
        credit.save
        update(credit_balance: credit.balance)
      end
    end
  end

  default_scope -> { order(created_at: :desc) }
end
