class User < ApplicationRecord
  include Accountable

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :payments
  has_many :withdrawals

  validates :email, uniqueness: true
  validates :phone, uniqueness: true

  validates_presence_of :email, :phone, :name, :surname, :patronymic
  validates :balance, numericality: { greater_than_or_equal_to: 0 }
end
