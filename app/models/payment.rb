class Payment < ApplicationRecord
  include Accountable
  include AASM

  belongs_to :user

  aasm column: :state do
    state :new, initial: true
    state :executed
    state :rejected

    event :reject do
      transitions from: :new, to: :rejected
    end

    event :execute do
      before do
        transaction = Transaction.create debit: self, credit: user, subject: self, sum: self.sum
        transaction.execute
      end

      transitions from: :new, to: :executed
    end
  end
end
