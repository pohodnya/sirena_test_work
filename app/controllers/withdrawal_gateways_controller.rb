class WithdrawalGatewaysController < ApplicationController

  add_flash_types :balance_error

  def create
    @withdrawal = Withdrawal.where(id: params[:withdrawal_id]).any? ? Withdrawal.where(id: params[:withdrawal_id]).first : nil
    if @withdrawal && @withdrawal.sum == params[:sum].to_i
      if @withdrawal.may_execute?
        @withdrawal.execute
        @withdrawal.save
        redirect_to @withdrawal
      else
        redirect_to withdrawals_path, balance_error: "На счету недостаточно средств!"
      end
    end
  end
end
