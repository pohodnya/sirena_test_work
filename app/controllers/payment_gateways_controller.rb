class PaymentGatewaysController < ApplicationController

  def create
    @payment = Payment.where(id: params[:payment_id]).any? ? Payment.where(id: params[:payment_id]).first : nil
    if @payment && @payment.sum == params[:sum].to_i
      @payment.execute
      @payment.save
    end
    redirect_to @payment
  end
end
