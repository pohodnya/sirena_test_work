class WithdrawalsController < ApplicationController

  add_flash_types :balance_error

  def index
    @withdrawals = current_user.withdrawals
  end

  def show
    @withdrawal = Withdrawal.where(id: params[:id]).first
  end


  def new
    @withdrawal = Withdrawal.new user_id: params[:user_id] || current_user.to_param
  end

  def create
    @withdrawal = Withdrawal.new(withdrawal_params)

    respond_to do |format|
      if @withdrawal.sum <= @withdrawal.user.balance
        if @withdrawal.save
          format.html { redirect_to @withdrawal, notice: "Вывод средств на сумму #{@withdrawal.sum} успешно создан" }
          format.json { render :show, status: :created, location: @withdrawal }
        else
          format.html { render :new }
          format.json { render json: @withdrawal.errors, status: :unprocessable_entity }
        end
      else
        format.html { redirect_to new_withdrawal_path, balance_error: "На счету недостаточно средств!"}
      end
    end
  end

  private

  def withdrawal_params
    params.require(:withdrawal).permit :sum, :user_id, :reason
  end
end
