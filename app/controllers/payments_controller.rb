class PaymentsController < ApplicationController

  def index
    @payments = current_user.payments
  end

  def show
    @payment = Payment.where(id: params[:id]).first
  end


  def new
    @payment = Payment.new user_id: params[:user_id] || current_user.to_param
  end

  def create
    @payment = Payment.new(payment_params)

    respond_to do |format|
      if @payment.save
        format.html { redirect_to @payment, notice: "Платеж на сумму #{@payment.sum} успешно создан" }
        format.json { render :show, status: :created, location: @payment }
      else
        format.html { render :new }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def payment_params
    params.require(:payment).permit :sum, :user_id
  end
end
