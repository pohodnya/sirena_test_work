class TransactionsController < ApplicationController
  def index
    @transactions = current_user.debit_transactions + current_user.credit_transactions
  end

  def show
    @transaction = Transaction.find(params[:id])
  end
end
